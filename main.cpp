#include <vector>
#include <lmdb++.h>

#include <iostream>
#include "product.h"

class product_function
{
public:
    virtual ~product_function() = default;
    virtual void apply(const kassenbuch::dto::product &product) = 0;
};

class product_repository
{
public:
    product_repository(lmdb::env &environment, lmdb::dbi &database)
        : m_database(database),
          m_environment(environment)
    {}

    void for_each(product_function &&function)
    {
        auto transaction = lmdb::txn::begin(m_environment, nullptr, MDB_RDONLY);
        auto cursor = lmdb::cursor::open(transaction, m_database);

        lmdb::val key, val;
        while (cursor.get(key, val, MDB_NEXT))
            function.apply(*val.data<kassenbuch::dto::product>());

        cursor.close();
        transaction.abort();
    }

    void put(const kassenbuch::dto::product &product)
    {
        auto transaction = lmdb::txn::begin(m_environment);

        lmdb::val key(product.code, 20);
        lmdb::val val(&product, sizeof(product));
        m_database.put(transaction, key, val);

        transaction.commit();
    }

private:
    lmdb::dbi &m_database;
    lmdb::env &m_environment;
};

template <typename UnaryFunction>
class product_function_adapter : public product_function
{
public:
    product_function_adapter(UnaryFunction function)
        : m_function(function)
    {}

    void apply(const kassenbuch::dto::product &product)
    {
        m_function(product);
    }

private:
    UnaryFunction m_function;
};

template <typename UnaryFunction>
product_function_adapter<UnaryFunction> make_product_function(UnaryFunction function)
{
    return product_function_adapter<UnaryFunction>(function);
}

// 

#include <nnxx/poll.h>
#include <nnxx/pair.h>
#include <nnxx/socket.h>
#include <nnxx/message.h>

class server 
{
public:
    server(product_repository &products)
        : m_socket(nnxx::SP, nnxx::PAIR),
          m_products(products)
    {}

    void listen(const char *address)
    {
        m_socket.bind(address);
    }

    void poll()
    {
        while (1)
        {
            auto m = m_socket.recv();
            m_products.put(*static_cast<kassenbuch::dto::product *>(m.data()));

            m_products.for_each(make_product_function([](const auto &p) {
                std::cout << "code: " << p.code << std::endl
                          << "name: " << p.name << std::endl 
                          << "text: " << p.text << std::endl
                          << "------" << std::endl;
            }));
        }
    }

private:
    nnxx::socket m_socket;
    product_repository &m_products;
};

int main(int argc, char **argv)
{
    // LMDB environment 
    auto env = std::move(lmdb::env::create().set_max_dbs(2));
    env.open("/var/lib/kassenbuch/", 0, 0664);

    // LMDB databases
    auto txn = lmdb::txn::begin(env);
    auto product_database = lmdb::dbi::open(txn, "products", MDB_CREATE);
    txn.commit();
    
    // Repositories
    product_repository products(env, product_database);

    // TCP server
    server s(products);
    s.listen("tcp://*:4000");
    s.poll();
}
