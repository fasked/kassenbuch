#pragma once

namespace kassenbuch
{
    namespace dto
    {
        struct product
        {
            char code[20];
            char name[100];
            char text[100];
        };
    }
}
