#include <cstring>
#include <nnxx/pair.h>
#include <nnxx/socket.h>

struct product
{
    char code[20];
    char name[100];
    char text[100];
};

int main(int argc, char **argv)
{
    if (argc == 4)
    {
        product product;
        std::strncpy(product.code, argv[1], 20);
        std::strncpy(product.name, argv[2], 100);
        std::strncpy(product.text, argv[3], 100);

        nnxx::socket socket(nnxx::SP, nnxx::PAIR);
        socket.connect("tcp://localhost:4000");
        socket.send(&product, sizeof(product), 0);
    }
}
