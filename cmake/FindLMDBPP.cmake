include(FindPackageHandleStandardArgs)

find_path(LMDBPP_INCLUDE_DIR NAMES lmdb++.h PATHS ${CMAKE_SOURCE_DIR}/3rd_party/lmdb++)
find_package_handle_standard_args(LMDBPP DEFAULT_MSG LMDBPP_INCLUDE_DIR)

mark_as_advanced(LMDBPP_INCLUDE_DIR)
